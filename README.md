# zen_installer

This is a fork of Zen Installer, a graphical installer for Arch Linux, but this one is thought for Parabola GNU/Linux-libre. With many options, the install you make is completely unique to you and requires minimal effort aside from making your choices. This fork is made to install an OpenRC system, currently. However, if requested, I can add an option to choose the init program.

Dependencies:

**Root**

pacman-contrib

gparted

zenity